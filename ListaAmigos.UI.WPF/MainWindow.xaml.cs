﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ListaAmigos.BIZ;
using ListaAmigos.DAL.NoSQL.LOCAL;
using ListaAmigos.COMMON.Entidades;
using System.IO;
using Microsoft.Win32;

namespace ListaAmigos.UI.WPF
{
	/// <summary>
	/// Lógica de interacción para MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		AmigoManager _manager;
		string accion = "nuevo";
		string direccionFoto = null;
		public MainWindow()
		{
			InitializeComponent();
			_manager = new AmigoManager(new AmigoRepository());
			ActualizarLista();
			LimpiarCajas();

		}

		private void ActualizarLista(List<AmigosDTO> datos = null)
		{
			lsvAmigos.ItemsSource = null;
			if (datos == null)
			{
				lsvAmigos.ItemsSource = _manager.ObtenerElementos;
			}
			else
			{
				lsvAmigos.ItemsSource = datos;
			}
		}

		private void LimpiarCajas()
		{
			imgFoto.Source = null;
			txtDireccion.Clear();
			txtEmail.Clear();
			txtNombre.Clear();
			txtNotas.Clear();
			txtSobrenombre.Clear();
			txtTelefono.Clear();
			dtpCalendario.SelectedDate = DateTime.Now;
			txtCriterio.Text = "";
			btnCancelar.IsEnabled = false;
			btnEditar.IsEnabled = true;
			btnEliminar.IsEnabled = true;
			btnGuardar.IsEnabled = false;
			btnNuevo.IsEnabled = true;
			HabilitarCajas(false);
		}

		private void btnBuscar_Click(object sender, RoutedEventArgs e)
		{
			LimpiarCajas();
			switch (cmbBuscar.Text)
			{
				case "Nombre":
					ActualizarLista(_manager.BuscarPorNombre(txtCriterio.Text));
					break;
				case "Apodo":
					ActualizarLista(_manager.BuscarPorApodo(txtCriterio.Text));
					break;
				case "Email":
					ActualizarLista(_manager.BuscarPorEmail(txtCriterio.Text));
					break;
				case "Teléfono":
					ActualizarLista(_manager.BuscarPorTelefono(txtCriterio.Text));
					break;
				default: ActualizarLista(_manager.BuscarPorTelefono(txtCriterio.Text));
					break;
			}
		}

		private void btnRestaurar_Click(object sender, RoutedEventArgs e)
		{
			ActualizarLista();
			LimpiarCajas();
		}

		private void btnNuevo_Click(object sender, RoutedEventArgs e)
		{
			LimpiarCajas();
			accion = "nuevo";
			btnCancelar.IsEnabled = true;
			btnEditar.IsEnabled = false;
			btnEliminar.IsEnabled = false;
			btnGuardar.IsEnabled = true;
			btnNuevo.IsEnabled = true;
			HabilitarCajas(true);
		}

		private void btnEditar_Click(object sender, RoutedEventArgs e)
		{
			if (lsvAmigos.SelectedItem != null)
			{
				AmigosDTO a = (AmigosDTO)lsvAmigos.SelectedItem;
				txtDireccion.Text = a.Direccion;
				txtEmail.Text = a.Email;
				txtNombre.Text = a.Nombre;
				txtSobrenombre.Text = a.Sobrenombre;
				txtTelefono.Text = a.Telefono;
				txtNotas.Text = a.Notas;
				dtpCalendario.SelectedDate = a.FechaNacimiento;
				if (a.StreamFoto != null)
				{
					BitmapImage bmpi = new BitmapImage();
					bmpi.BeginInit();
					bmpi.StreamSource = new MemoryStream(a.StreamFoto);
					bmpi.EndInit();
					imgFoto.Source = bmpi;
				}
				accion = "editar";
				btnCancelar.IsEnabled = true;
				btnEditar.IsEnabled = false;
				btnEliminar.IsEnabled = false;
				btnGuardar.IsEnabled = true;
				btnNuevo.IsEnabled = true;
				HabilitarCajas(true);
			}
		}

		private void btnGuardar_Click(object sender, RoutedEventArgs e)
		{
			AmigosDTO a = new AmigosDTO();
			a.Direccion = txtDireccion.Text;
			a.Email = txtEmail.Text;
			a.Nombre = txtNombre.Text;
			a.Telefono = txtTelefono.Text;
			a.Notas = txtNotas.Text;
			a.Sobrenombre = txtSobrenombre.Text;
			a.FechaNacimiento = (DateTime)dtpCalendario.SelectedDate;
			a.IdFotografia = direccionFoto;
			if (accion == "nuevo")
			{
				if (MessageBox.Show("¿Realmente deseas agregar este amigo?", "Confirma", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
				{
					try
					{
						a.StreamFoto = ObtenerFoto(a);
						if (_manager.Agregar(a) != null)
						{
							MessageBox.Show("Amigo agregado correctamente", "Resultado", MessageBoxButton.OK, MessageBoxImage.Information);
							ActualizarLista();
							LimpiarCajas();
							direccionFoto = "";
						}
						else
						{
							MessageBox.Show("Tu amigo no puede ser guardado", "Resultado", MessageBoxButton.OK, MessageBoxImage.Information);
						}
					}
					catch (Exception ex)
					{
						MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
						throw;
					}
				}
			}
			else
			{
				if (MessageBox.Show("¿Realmente deseas modificar este contacto?", "Confirmar", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
				{
					try
					{
						a.StreamFoto = ObtenerFoto(a);
						a.Id = ((AmigosDTO)lsvAmigos.SelectedItem).Id;
						if (_manager.Modificar(a) != null)
						{
							MessageBox.Show("Amigo modificado correctamente", "Resultado", MessageBoxButton.OK, MessageBoxImage.Information);
							ActualizarLista();
							LimpiarCajas();
							direccionFoto = "";
						}
						else
						{
							MessageBox.Show("El vontacto no puede ser modificado", "Resultado", MessageBoxButton.OK, MessageBoxImage.Information);
						}
					}
					catch (Exception ex)
					{
						MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
					}
				}
			}
		}

		private void btnEliminar_Click(object sender, RoutedEventArgs e)
		{
			if (lsvAmigos.SelectedItem != null)
			{
				AmigosDTO a = (AmigosDTO)lsvAmigos.SelectedItem;
				if (MessageBox.Show("¿Realmente deseas eliminar a " + a.Nombre + "?", "Confirmar", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
				{
					if (_manager.Eliminar((AmigosDTO)lsvAmigos.SelectedItem))
					{
						MessageBox.Show("Amigo eliminado de forma correcta", "Resultado", MessageBoxButton.OK, MessageBoxImage.Information);
						ActualizarLista();
					}
					else
					{
						MessageBox.Show("Error al eliminar al contacto", "Resultado", MessageBoxButton.OK, MessageBoxImage.Error);
						ActualizarLista();
					}
				}
			}
		}

		private void btnCancelar_Click(object sender, RoutedEventArgs e)
		{
			LimpiarCajas();
		}

		private void HabilitarCajas(bool status)
		{
			txtDireccion.IsEnabled = status;
			txtEmail.IsEnabled = status;
			txtNombre.IsEnabled = status;
			txtTelefono.IsEnabled = status;
			txtSobrenombre.IsEnabled = status;
			txtNotas.IsEnabled = status;
			dtpCalendario.IsEnabled = status;
		}

		private void btnBuscarFoto_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog dialogo = new OpenFileDialog();
			dialogo.Title = "Buscar fotografía";
			dialogo.Filter = "Imágenes JPG PNG| *jpg; *.png";
			if((bool)dialogo.ShowDialog())
			{
				imgFoto.Source = new BitmapImage(new Uri(dialogo.FileName));
				direccionFoto = dialogo.FileName;
			}
			else
			{
				direccionFoto = null;
			}
		}

		private byte[] ObtenerFoto(AmigosDTO a)
		{
			if (direccionFoto == null)
			{
				return a.StreamFoto;
			}
			else
			{
				if (direccionFoto != null)
				{
					JpegBitmapEncoder encoder = new JpegBitmapEncoder();
					encoder.Frames.Add(BitmapFrame.Create(new Uri(direccionFoto)));
					MemoryStream ms = new MemoryStream();
					encoder.Save(ms);
					return ms.ToArray();
				}
				else
				{
					return a.StreamFoto;
				}
			}
		}
	}
}