﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ListaAmigos.COMMON.Entidades;

namespace ListaAmigos.COMMON.Interfaces
{
	public interface IAmigoManager : IGenericManager<AmigosDTO>
	{
		List<AmigosDTO> BuscarPorNombre(string nombre);
		List<AmigosDTO> BuscarPorApodo(string apodo);
		List<AmigosDTO> BuscarPorEmail(string email);
		List<AmigosDTO> BuscarPorTelefono(string telefono);
	}
}
