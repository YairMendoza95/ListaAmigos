﻿using System.Collections.Generic;

namespace ListaAmigos.COMMON.Interfaces
{
	public interface IGenericRepository<T> where T : class
	{
		T Create(T entidad);
		List<T> ReadItems { get; set; }
		T Update(T entidad);
		bool Delete(T entidad);
	}
}
