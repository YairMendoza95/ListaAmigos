﻿using System.Collections.Generic;

namespace ListaAmigos.COMMON.Interfaces
{
	public interface IGenericManager<T> where T : class
	{
		T Agregar(T entidad);
		List<T> ObtenerElementos { get; set; }
		T Modificar(T entidad);
		bool Eliminar(T entidad);
	}
}
