﻿using System;

namespace ListaAmigos.COMMON.Entidades
{
	public abstract class BaseDTO : IDisposable
	{
		private bool _isDisposed;
		public void Dispose()
		{
			if (!_isDisposed)
			{
				this._isDisposed = true;
				GC.SuppressFinalize(this);
			}
		}
	} 
}
