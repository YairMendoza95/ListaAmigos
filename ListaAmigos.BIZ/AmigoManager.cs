﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ListaAmigos.COMMON.Entidades;
using ListaAmigos.COMMON.Interfaces;

namespace ListaAmigos.BIZ
{
	public class AmigoManager : IAmigoManager
	{
		private IGenericRepository<AmigosDTO> _repositorio;

		public AmigoManager()
		{
		}

		public AmigoManager(IGenericRepository<AmigosDTO> repositorio)
		{
			_repositorio = repositorio;
		}
		public List<AmigosDTO> ObtenerElementos
		{
			get
			{
				return _repositorio.ReadItems;
			}

			set
			{
			}
		}

		public AmigosDTO Agregar(AmigosDTO entidad)
		{
			return _repositorio.Create(entidad);
		}

		public List<AmigosDTO> BuscarPorApodo(string apodo)
		{
			return ObtenerElementos.Where(a => a.Sobrenombre.Contains(apodo)).ToList();
		}

		public List<AmigosDTO> BuscarPorEmail(string email)
		{
			return ObtenerElementos.Where(a => a.Email.Contains(email)).ToList();
		}

		public List<AmigosDTO> BuscarPorNombre(string nombre)
		{
			return ObtenerElementos.Where(a => a.Nombre.Contains(nombre)).ToList();
		}

		public List<AmigosDTO> BuscarPorTelefono(string telefono)
		{
			return ObtenerElementos.Where(a => a.Telefono.Contains(telefono)).ToList();
		}

		public bool Eliminar(AmigosDTO entidad)
		{
			return _repositorio.Delete(entidad);
		}

	public AmigosDTO Modificar(AmigosDTO entidad)
		{
			return _repositorio.Update(entidad);
		}
	}
}
