﻿using System;
using System.Collections.Generic;
using System.Linq;
using LiteDB;
using ListaAmigos.COMMON.Entidades;
using ListaAmigos.COMMON.Interfaces;
using System.IO;

namespace ListaAmigos.DAL.NoSQL.LOCAL
{
	public class AmigoRepository : IGenericRepository<AmigosDTO>
	{
		private string _dbName = "Amigos.db";
		private string _tabName = "amigos";

		public List<AmigosDTO> ReadItems
		{
			get
			{
				List<AmigosDTO> datos;
				using (var db = new LiteDatabase(_dbName))
				{
					datos = db.GetCollection<AmigosDTO>(_tabName).FindAll().ToList<AmigosDTO>();
				}
				return datos;
			}

			set
			{
			}
		}

		public AmigosDTO Create(AmigosDTO entidad)
		{
			entidad.Id = Guid.NewGuid().ToString();
			using (var db = new LiteDatabase(_dbName))
			{
				var colection = db.GetCollection<AmigosDTO>(_tabName);
				if(entidad.IdFotografia != null)
				{
					db.FileStorage.Upload(entidad.Id, entidad.IdFotografia, new MemoryStream(entidad.StreamFoto));
					entidad.IdFotografia = entidad.Id;
				}
				colection.Insert(entidad);
			}
			return entidad;

		}

		public bool Delete(AmigosDTO entidad)
		{
			int r;
			using (var db = new LiteDatabase(_dbName))
			{
				if (entidad.IdFotografia != null)
				{
					db.FileStorage.Delete(entidad.IdFotografia);
				}
				var colection = db.GetCollection<AmigosDTO>(_tabName);
				r = colection.Delete(e => e.Id == entidad.Id);
			}
			return r > 0 ? true : false;
		}

		public AmigosDTO Update(AmigosDTO entidad)
		{
			bool r;
			using (var db = new LiteDatabase(_dbName))
			{
				var colection = db.GetCollection<AmigosDTO>(_tabName);
				if (entidad.IdFotografia != null)
				{
					db.FileStorage.Upload(entidad.Id, entidad.IdFotografia, new MemoryStream(entidad.StreamFoto));
				}
				entidad.IdFotografia = entidad.Id;
				r = colection.Update(entidad);
			}
			if (r)
			{
				return entidad;
			}
			else
			{
				return null;
			}
		}
	}
}
